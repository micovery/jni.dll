#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <crtdbg.h> 
#include "jni.h"
#include "string.h"
#include "direct.h"
#include <time.h>

#define MAX_LIB_PATH 2048
#define DLL_NAME L"jvm.dll"
#define DSP L"\\"
#define LSP "\n"
#define DLL_PATH  L"bin" DSP L"client" DSP DLL_NAME
static JNIEnv * env = NULL;
static HINSTANCE jvm_dll = NULL;
static int classes_loaded = 0;

#define MAX_ENV_NAME_SZ 1024
#define MAX_ENV_VAL_SZ 1024
#define MAX_LINE_SZ 4096
#define EQUAL '='
#define LT '\n'
#define CONFIG_FILE "jni.conf"

#define LOG_TIME_FORMAT "%b %d %H:%M:%S"
#define LOG_INFO(format, ...) log && (fprintf(log, "%s INFO: "  format, timestamp(), __VA_ARGS__))
#define LOG_WARNING(format, ...) log && (fprintf(log, "%s WARNING: "  format, timestamp(), __VA_ARGS__))
#define LOG_ERROR(format, ...) log && (fprintf(log, "%s ERROR: "  format, timestamp(), __VA_ARGS__))
#define LOG_ERROR_CONFIG(p) LOG_ERROR("%s parameter was not provided in config file\n", #p)


static char * CLASSPATH = NULL;
static char * MAINCLASS = NULL;
static char * JAVA_HOME = NULL;
static char * JDWP_ADDRESS = NULL;
static FILE * log = NULL;
static char time_buffer[100];


char * timestamp() {
    time_t ltime; 
    ltime=time(NULL); 
	strftime(time_buffer, sizeof(time_buffer), LOG_TIME_FORMAT, localtime(&ltime));
	return time_buffer;
}

void open_log() {
	log = fopen("jni.log", "w+");
	setvbuf(log, NULL, _IONBF, 0);
}

void close_log() {
	if (log) 
		fclose(log);
}

void load_config() {
	char linebuf[MAX_LINE_SZ];
	char left[MAX_ENV_NAME_SZ];
	char right[MAX_ENV_VAL_SZ];


	FILE * fp = fopen(CONFIG_FILE, "r");
	if (!fp) {
		LOG_ERROR("Configuration file %s was not found\n", CONFIG_FILE);
		return;
	}

	while (fgets(linebuf, sizeof(linebuf), fp)) {

		int i = 0, j = 0;
		char * part = left;
		for (; i < sizeof(linebuf) && j < sizeof(left) - 1; i++) {
			char chr = linebuf[i];
			if (chr == LT || chr == '\0')
				break;
			if (chr == EQUAL) {
				part[j] = 0;
				part = right;
				j = 0;
				continue;
			}
			part[j++] = chr;
		}
		part[j] = 0;

		if (part == left) {
			//fprintf(log, "INFO: [SKIPPED]: %s", linebuf);
			LOG_INFO("[SKIPPED]: %s", linebuf);
			continue;
		}

	
		int rlen = strnlen(right, sizeof(right)) + 1;
		char * right_copy = (char*) calloc(rlen, sizeof(char));

		
		strncat_s(right_copy, rlen, right, rlen - 1);
		
		int ignored = 0;
		if (strcmp(left, "CLASSPATH") == 0) {
			CLASSPATH = right_copy;
		}
		else if (strcmp(left,"MAINCLASS") == 0) {
			//replace . by /
			for (i = 0; i < rlen; i++) 
				if (right_copy[i] == '.')
					right_copy[i] = '/';
			MAINCLASS = right_copy;
		}
		else if (strcmp(left, "JAVA_HOME") == 0) {
			JAVA_HOME = right_copy;
		}
		else if (strcmp(left, "JDWP_ADDRESS") == 0) {
			JDWP_ADDRESS = right_copy;
		}
		else {
			ignored = 1;
		}
		LOG_INFO("[%s]: %s=%s\n",((ignored)?"IGNORED":"LOADED"), left, right_copy);
	}

	fclose(fp);
}



int validate_config() {
	int valid = 1;
	if (!CLASSPATH) {
		LOG_ERROR_CONFIG(CLASSPATH);
		valid = 0;
	}

	if (!MAINCLASS) {
		LOG_ERROR_CONFIG(MAINCLASS);
		valid = 0;
	}

	if (!JAVA_HOME) {
		LOG_ERROR_CONFIG(JAVA_HOME);
		valid = 0;
	}

	return valid;
}

void load_jvm() {
	if (!validate_config()) {
		LOG_ERROR("refusing to load JVM dll because config file is not valid\n");
		return;
	}
	wchar_t lib_path[1024] = {0};

	//wchar_t * java_home = _wgetenv(L"JNI_JAVA_HOME");
	wchar_t  java_home[1024];
	swprintf(java_home, sizeof(java_home), L"%hs", JAVA_HOME);


	wcsncat_s(lib_path, java_home, MAX_LIB_PATH);
	wcsncat_s(lib_path, DSP, MAX_LIB_PATH);
	wcsncat_s(lib_path, DLL_PATH, MAX_LIB_PATH);

	jvm_dll = LoadLibrary(lib_path);
}

void unload_jvm() {
	if (jvm_dll)
		FreeLibrary(jvm_dll);
}

BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
		case DLL_PROCESS_DETACH:
			unload_jvm();
			close_log();
			break;
		case DLL_PROCESS_ATTACH:
			open_log();
			load_config();
			load_jvm();
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
	}
	return TRUE;
}

extern "C" {  __declspec(dllexport) void __stdcall RVExtension(char *output, int outputSize, const char *function); };
typedef UINT (CALLBACK* CreateJavaVM_fptr)( JavaVM**, void**, void* );
typedef UINT (CALLBACK* GetDefaultJavaVMInitArgs_fptr)( void* );
typedef UINT (CALLBACK* GetCreatedJavaVMs_fptr)( JavaVM**, jsize, jsize* );
#define ERROR_EXIT { env = NULL; goto EXIT; }
#define MAX_CLASSPATH_SZ 512
#define CLASSPATH_PREFIX "-Djava.class.path"
#define MAX_JDWP_SZ 64
#define JDWP_PREFIX "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address"
#define MAX_FN 1024
#define ATXT DSP L"*"
#define sfc(dst, dsts, src) { wcstombs(src, fd.cFileName, MAX_FN); if ((strlen(src) + strlen(dst) + sizeof(LSP)) > dsts) return NULL;  strncat_s(dst, dsts, src, strlen(src)); strncat_s(dst, dsts, LSP, strlen(LSP)); }


JNIEnv * init_jvm(char const * entry, char * out, size_t outs) {

	if (env)
		goto EXIT;

	if (!jvm_dll) {
		LOG_ERROR("JVM dll was not loaded\n");
		ERROR_EXIT;
	}

	CreateJavaVM_fptr CreateJavaVM = (CreateJavaVM_fptr) GetProcAddress(jvm_dll, "JNI_CreateJavaVM");
	if (!CreateJavaVM)  {
		LOG_ERROR("Could not find address of JNI_CreateJavaVM in dll\n");
		ERROR_EXIT;
	}

	GetCreatedJavaVMs_fptr GetCreatedJavaVMs = (GetCreatedJavaVMs_fptr) GetProcAddress(jvm_dll, "JNI_GetCreatedJavaVMs");
	if (!GetCreatedJavaVMs) {
		LOG_ERROR("Could not find address of JNI_GetCreatedJavaVMs in dll\n");
		ERROR_EXIT;
	}
	
	JavaVM* jvm;
	jsize jvms_sz = 1;
	JavaVM* jvms[1];
	jsize njvms = 0;

	if (GetCreatedJavaVMs(jvms, jvms_sz, &njvms) == 0 && njvms > 0) {
		jvm = jvms[0];
		if ((jvm->AttachCurrentThread((void **) &env, NULL)) < 0) {
			LOG_ERROR("Could not attach to existing JVM\n");
			ERROR_EXIT;
		}
	}
	else {
		JavaVMInitArgs args;
		JavaVMOption options[5];
	
		char * class_path_env = CLASSPATH;
		char * class_path = (char *) calloc(MAX_CLASSPATH_SZ, sizeof(char));
		strncat_s(class_path, MAX_CLASSPATH_SZ, CLASSPATH_PREFIX, sizeof(CLASSPATH_PREFIX)-1);
		strncat_s(class_path, MAX_CLASSPATH_SZ, "=", 1);
		strncat_s(class_path, MAX_CLASSPATH_SZ, class_path_env, strlen(class_path_env));
		options[0].optionString = class_path;
	

		options[1].optionString="-verbose:jni";
		options[2].optionString="-Djava.compiler=NONE";
		options[3].optionString="-Xdebug";


		char * jdwp_address_env = JDWP_ADDRESS;
		if (jdwp_address_env) {
			char * jdwp = (char *) calloc(MAX_JDWP_SZ, sizeof(char));
			strncat_s(jdwp, MAX_JDWP_SZ, JDWP_PREFIX, sizeof(JDWP_PREFIX)-1);
			strncat_s(jdwp, MAX_JDWP_SZ, "=", 1);
			strncat_s(jdwp, MAX_JDWP_SZ, jdwp_address_env, strlen(jdwp_address_env));
			options[4].optionString=jdwp;
		}
		else 
			options[4].optionString="-Dnop";

		args.version =  JNI_VERSION_1_6;
		args.nOptions = 5;
		args.ignoreUnrecognized = JNI_FALSE;
		args.options = options;

		if (CreateJavaVM(&jvm, (void **) &env, &args) < 0)
			ERROR_EXIT
	}

EXIT:
	WIN32_FIND_DATA fd;
	HANDLE h = INVALID_HANDLE_VALUE;
	int dlen = 0;
	wchar_t* df = NULL;
	char fn[MAX_FN] = {0};
	int length = 0;

	if (!(entry && *entry == '*'))
		return env;

	outs = outs - 1;
	entry = entry + 1;
	length = strlen(entry);

	dlen = length * 2 + sizeof(ATXT);
	df = (wchar_t *) calloc(dlen + 1, sizeof(wchar_t));
	mbstowcs(df, entry, length);
	wcsncat_s(df, dlen, ATXT, sizeof(ATXT));

	if ((h = FindFirstFile(df, &fd)) == INVALID_HANDLE_VALUE) {
		sprintf_s(out, outs, "error: %u", GetLastError());
		return NULL;
	} 
	
	sfc(out, outs, fn);
	while (FindNextFile(h, &fd)) 
		sfc(out, outs, fn);

	return NULL;
}


void ignore(const wchar_t* expression, const wchar_t* function, const wchar_t* file, unsigned int line, uintptr_t pReserved) {
	return;
}

void __stdcall RVExtension(char *output, int outputSize, const char *entry) {
	LOG_INFO("RVExtension(%s)\n", entry);
	_invalid_parameter_handler handler = _set_invalid_parameter_handler(ignore);
	_CrtSetReportMode(_CRT_ASSERT, 0);

	if (!init_jvm(entry, output, outputSize)) {
		LOG_ERROR("JVM could not be initialized\n");
		goto EXIT;
	}

	jclass myClass;
	jmethodID myMethod;
	jstring argument;
	jstring result;
	const char * result_data;

	char * main_class = MAINCLASS;

	if (!main_class) {
		LOG_ERROR("MAINCLASS parameter is not configured\n");
		goto EXIT;
	}

	if ((myClass = env->FindClass(main_class)) == NULL) {
		LOG_ERROR("Could not find class %s in CLASSPATH", main_class);
		goto EXIT;
	}

	if ((myMethod = env->GetStaticMethodID(myClass,"run","(Ljava/lang/String;)Ljava/lang/String;")) == NULL) {
		LOG_ERROR("Class %s does not contain a method with signature: \"public static String run(String arg)\"\n", MAINCLASS);
		goto EXIT;
	}

	if ((argument = env->NewStringUTF(entry)) == NULL)
		goto EXIT;


	if ((result = (jstring) env->CallStaticObjectMethod(myClass, myMethod, argument)) == NULL) {
		LOG_ERROR("The \"run\" method returned NULL\n");
		goto EXIT;
	}

	jboolean isCopy;
	if ((result_data = env->GetStringUTFChars(result, &isCopy)) == NULL)
		goto EXIT;


	int result_sz = strnlen(result_data, outputSize);
	strncpy_s(output, outputSize, result_data, result_sz);

	if (isCopy)
		env->ReleaseStringUTFChars(result, result_data);
	
	int i = result_sz;
	while (i < outputSize) 
		output[i++] = NULL;

	LOG_INFO("RVExtension(%s): %s\n", entry, output);

EXIT:
	if (handler)
	   _set_invalid_parameter_handler(handler);
	return;
}


